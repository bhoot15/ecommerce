<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],	
		
	'facebook' => [
		'client_id' => '2077841532544529',
		'client_secret' => '2ca58997b92ff32aa5bb473fa9f6735a',
		'redirect' => 'https://anonderbazar.com/login/facebook/callback',
	],
	
	
	'google' => [
		'client_id' => '70466316215-nihjdqahhglvbjop2orrockmj5jah0r2.apps.googleusercontent.com',
		'client_secret' => 'rDtxka9IYPLm2bz_tMf1tlli',
		'redirect' => 'https://anonderbazar.com/login/google/callback',
	],

];
